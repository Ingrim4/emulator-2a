# Emulator for the mini computer 2a

This is an angular based emulator fo the mini computer 2a which is part of the module "Grundlagen der Technischen Informatik 2". The mini-computer 2a is an 8-bit computer that implements the Neumann architecture.

## Manual

Sadly I don't own the manual anymore which is why I can't link it here. 😢

## Demo

I have a running version on my server: https://ingrim4.me/emulator/
You're welcome to try it out.
import { Component } from '@angular/core';
import { CoreService } from './2a/core.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'emulator';

  constructor(
    private core: CoreService
  ) { }
}

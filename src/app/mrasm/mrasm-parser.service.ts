import { Injectable } from '@angular/core';
import { CharBuffer } from './char-buffer';
import { IoAddress, IoAddressType } from '../2a/io/io-address';
import { ParsedObject } from './mrasm-parser.interface';

@Injectable({
  providedIn: 'root'
})
export class MrasmParserService {

  parse(input: string) {
    const parsed: ParsedObject = {
      labels: {},
      assemblerInstructions: [],
      instructions: [],
      unparsedInstructions: []
    };
    let line = 0;

    const lines = input.split(/\r?\n/);
    if (/(\s+)?#! mrasm(\s+)?/i.test(lines[0])) {
      lines.shift();
      line = 1;
      // throw new SyntaxError('wrong assambler expected "#! mrasm" to be in the 1. line');
    }

    while (lines.length > 0) {
      this.parseLine(lines.shift().trim(), parsed, line++);
    }

    for (let i = 0; i < parsed.unparsedInstructions.length; i++) {
      const instruction = parsed.unparsedInstructions[i];

      if (instruction.text[1].length === 0) {
        parsed.instructions[i] = {
          originalLine: instruction.line,
          name: instruction.text[0],
          dst: null,
          src: null
        };
        continue;
      }

      const args = instruction.text[1].replace(/\s+/g, '').split(/,/);
      if (args.length > 2) {
        throw new SyntaxError('too many arguments');
      }

      try {
        parsed.instructions[i] = {
          originalLine: instruction.line,
          name: instruction.text[0],
          dst: args.length >= 1 ? this.parseArgument(args[0], parsed, instruction.line + 1) : null,
          src: args.length >= 2 ? this.parseArgument(args[1], parsed, instruction.line + 1) : null
        };
      } catch (error) {
        console.log(parsed);
        throw error;
      }
    }

    return parsed;
  }

  parseLine(value: string, parsed: ParsedObject, line: number) {
    value = value.trim();
    if (value.length === 0 || value[0] === ';') {
      return;
    }

    const comment = /([^;#]*)/i.exec(value);
    if (!comment) {
      return;
    }

    value = comment[1];

    if (value[0] === '.') {
      parsed.assemblerInstructions.push({
        text: value.substr(1),
        line: parsed.unparsedInstructions.length
      });
      return;
    }

    if (value[value.length - 1] === ':') {
      parsed.labels[value.substr(0, value.length - 1)]
        = parsed.unparsedInstructions.length;
      return;
    }

    const instruction = /([a-z]+)\s*([^;]*)/i.exec(value) as string[];
    if (!instruction) {
      throw new SyntaxError(`invalid charsequence at (${line}, ${value}) expected '([a-z]+)\s*([^;]*)'`);
    }
    parsed.unparsedInstructions.push({ line, text: instruction.slice(1) });
  }

  parseArgument(value: string, parsed: ParsedObject, line: number) {
    const buffer: CharBuffer = new CharBuffer(value);
    if (this.matchBrackets(buffer, 0, line)) {
      if (this.matchBrackets(buffer, 1, line)) {
        if (buffer.end(2) !== '+') {
          throw new SyntaxError(`invalid char at (${line}, '${buffer.end(2)}') expected '+'`);
        }
        // REGISTER_ID_DOUBLE
        return new IoAddress(IoAddressType.REGISTER_DOUBLE_INDIRECT, this.parseRegister(buffer.range(2, 3), line));
      }
      if (buffer.end(1) === '+') {
        // REGISTER_ID_INC
        return new IoAddress(IoAddressType.REGISTER_INDIRECT_INCREMENT, this.parseRegister(buffer.range(1, 2), line));
      } else {
        if (buffer.start(1).toUpperCase() === 'R') {
          // REGISTER_ID
          return new IoAddress(IoAddressType.REGISTER_INDIRECT, this.parseRegister(buffer.range(1, 1), line));
        } else {
          // ADDRESS
          return new IoAddress(IoAddressType.ADDRESS, this.parseNumber(buffer.range(1, 1), line));
        }
      }
    }
    if (buffer.start(0).toUpperCase() === 'R') {
      // REGISTER
      return new IoAddress(IoAddressType.REGISTER, this.parseRegister(buffer.value, line));
    } else {
      const index = parsed.instructions.length;
      const instruction = parsed.unparsedInstructions[index].text[0];

      // CONST
      const label = this.parseLabel(buffer.value);
      if (label === buffer.value) {
        if (!(label in parsed.labels)) {
          throw new SyntaxError(`unknown label at (${line}, ${label})`);
        }
        if (instruction === 'JMP' || instruction === 'CALL') {
          return new IoAddress(IoAddressType.CONST, parsed.labels[label]);
        } else {
          return new IoAddress(IoAddressType.CONST, parsed.labels[label] - (index + 1));
        }
      } else {
        return new IoAddress(IoAddressType.CONST, this.parseNumber(buffer.value, line));
      }
    }
  }

  parseLabel(value: string) {
    const label = /([a-z_]+)/i.exec(value);
    if (label !== null) {
      return label[1];
    }
    return null;
  }

  parseNumber(value: string, line: number) {
    const exec = /^0x([0-9A-F]+)|0o([0-7]+)|0b([0-1]+)|(-?[0-9]+)\b/i.exec(value);
    if (exec === null) {
      throw new SyntaxError(`invalid charsequence at (${line}, ${value}) expected '0x([0-9A-F]+)|0o([0-7]+)|0b([0-1]+)|(-?[0-9]+)'`);
    }
    if (exec[1]) {
      return parseInt(exec[1], 16);
    } else if (exec[2]) {
      return parseInt(exec[2], 8);
    } else if (exec[3]) {
      return parseInt(exec[3], 2);
    } else if (exec[4]) {
      return parseInt(exec[4], 10);
    }
  }

  parseRegister(value: string, line: number): number {
    const register = /^R([0-3])\b/i.exec(value);
    if (register === null) {
      throw new SyntaxError(`invalid charsequence at (${line}, ${value}) expected 'R[0-3]'`);
    }
    return parseInt(register[1], 10);
  }

  matchBrackets(buffer: CharBuffer, offset: number, line: number): boolean {
    if (buffer.start(offset) === '(') {
      const end = buffer.end(offset);
      if (end !== ')') {
        throw new SyntaxError(`invalid char at (${line}, '${end}') expected ')'`);
      }
      return true;
    }
    return false;
  }
}

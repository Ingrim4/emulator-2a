import { TestBed } from '@angular/core/testing';

import { MrasmParserService } from './mrasm-parser.service';

describe('MrasmParserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const parser: MrasmParserService = TestBed.get(MrasmParserService);
    expect(parser).toBeTruthy();
  });

  it('should parse', () => {
    const parser: MrasmParserService = TestBed.get(MrasmParserService);
    parser.parse(`#! mrasm

    .ORG 0
      CLR R0

    LOOP:
      ST (0xFF),R0
      ST -5234,(0x11)
      ST (0b10101),(R0)
      ST (0o1527),(R0+)
      ST (14532),((R0+))
      INC R0
      JR LOOP
      RET

    `);
  });
});

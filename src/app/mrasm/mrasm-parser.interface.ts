import { ParameterizedInstruction } from '../2a/instruction/instruction.interface';

export interface ParsedObject {
  labels: { [key: string]: number; };
  assemblerInstructions: ParsedAssemblerInstruction[];
  instructions: ParsedInstruction[];
  unparsedInstructions: {
    line: number;
    text: string[];
  }[];
}

export interface ParsedAssemblerInstruction {
  text: string;
  line: number;
}

export interface ParsedInstruction extends ParameterizedInstruction {
  originalLine: number;
}

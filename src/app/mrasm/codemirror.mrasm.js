(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
  mod(require("codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
  define(["codemirror"], mod);
  else // Plain browser env
  mod(CodeMirror);
})(function(CodeMirror) {
"use strict";

CodeMirror.defineMode('mrasm', function(_config, parserConfig) {
  const keywords = /^(clr|ad[dc]|sub|mul|div|inc|dec|neg|and|x?or|com|bit[sct]|tst|cmp|[la]sr|lsl|r[rl]c|mov|st|ld|pushf?|popf?|ld(sp|fr)|stop|nop|[ed]i|call|j([czn][sc]|r|mp)|reti?)\b/i;
  const variables = /^(R[0-3])\b/i;
  const numbers = /^(0x([0-9a-f]+)|0o([0-7]+)|0b([0-1]+)|(-?[0-9]+))\b/i;
  return {
    startState() {
      return {
        context: 0
      };
    },
    token(stream, state) {
      if (!stream.column()) {
        state.context = 0;
      }

      if (stream.eatSpace()) {
        return null;
      }

      if (stream.eat('#') && stream.eat('!') && stream.eat(/\s/) && stream.eatWhile(/\w/) && stream.current() === '#! mrasm') {
        return 'comment';
      } else if (stream.eatWhile(/\w/)) {
        const word = stream.current();
        if (stream.indentation()) {
          if (keywords.test(word)) {
            state.context = 1;
            return 'keyword';
          } else if (state.context === 1 && numbers.test(word)) {
            return 'number';
          } else if (state.context === 1 &&variables.test(word)) {
            return 'variable';
          } else if (state.context === 1) {
            return 'string';
          }
        } else if (numbers.test(word)) {
          return 'number';
        } else if (stream.peek() === ':') {
          return 'string';
        }
        return null;
      } else if (stream.eat(';')) {
        stream.skipToEnd();
        return 'comment';
      } else if (stream.eat('.')) {
        if (stream.eatWhile(/\w/)) {
          return 'def';
        }
      } else {
        stream.next();
      }
    }
  };
});
});

export class CharBuffer {

  readonly value: string;

  constructor(value: string) {
    this.value = value;
  }

  get length() {
    return this.value.length;
  }

  start(offset: number) {
    if (this.value.length < 0) {
      throw new Error('index out of range');
    }
    return this.value[offset];
  }

  range(offsetStart: number, offsetEnd: number) {
    return this.value.substr(offsetStart, this.value.length - (offsetEnd + 1));
  }

  end(offset: number) {
    if (this.value.length <= offset) {
      throw new Error('index out of range');
    }
    return this.value[this.value.length - (offset + 1)];
  }
}

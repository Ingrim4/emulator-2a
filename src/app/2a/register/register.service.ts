import { Injectable } from '@angular/core';
import { RegisterHandler } from './register-handler.interface';
import { RegisterHandlerContext } from './register-handler-context';
import { InternalRegister } from './handler/internal-register';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor() {
    this.registerRegister(6, InternalRegister.INSTANCE);
    this.registerRegister(7, InternalRegister.INSTANCE);
  }

  public readonly registerArray: Uint8Array = new Uint8Array(8);

  private readonly registerHandlers: RegisterHandlerContext[] = [];

  /**
   * Internal method which throws a {@link RangeError} when the addressed register doesn't exist
   * @param register register to check
   */
  private checkRegister(register: number) {
    if (register < 0 || register > 7) {
      throw new Error(`register(${register}) out of range [0, 7]`);
    }
  }

  registerRegister(registerId: number, register: RegisterHandler) {
    this.checkRegister(registerId);

    if (typeof this.registerHandlers[registerId] !== 'undefined') {
      throw new Error(`Can't overwrite existing register at (${registerId})`);
    }

    register.registered(this.registerHandlers[registerId] = new RegisterHandlerContext(
      this.registerArray, registerId, register
    ));
  }

  read(register: number): number {
    this.checkRegister(register);

    const context = this.registerHandlers[register];
    if (typeof context !== 'undefined') {
      return context.register.read(context);
    }

    return this.registerArray[register];
  }

  write(register: number, value: number): void {
    this.checkRegister(register);

    const context = this.registerHandlers[register];
    if (typeof context !== 'undefined') {
      return context.register.write(context, value);
    }

    this.registerArray[register] = value;
  }
}

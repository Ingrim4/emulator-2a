import { RegisterHandlerContext } from './register-handler-context';

describe('RegisterContext', () => {
  it('should create an instance', () => {
    expect(new RegisterHandlerContext(new Uint8Array(1), 0, null)).toBeTruthy();
    expect(() => new RegisterHandlerContext(new Uint8Array(0), 0, null)).toThrowError(
      'address(0) is bigger or equal to registerArray.length(0)');
  });

  it('should read and write', () => {
    const registerArray = new Uint8Array(8);
    const context = new RegisterHandlerContext(registerArray, 3, null);

    expect(context.read()).toEqual(0);

    context.write(1);

    expect(context.read()).toEqual(1);
  });
});

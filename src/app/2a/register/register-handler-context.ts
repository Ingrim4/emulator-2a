import { RegisterHandler } from './register-handler.interface';

export class RegisterHandlerContext {

  private readonly registerArray: Uint8Array;
  private readonly address: number;
  public readonly register: RegisterHandler;

  constructor(registerArray: Uint8Array, address: number, register: RegisterHandler) {
    if (address >= registerArray.length) {
      throw new Error(`address(${address}) is bigger or equal to registerArray.length(${registerArray.length})`);
    }
    this.registerArray = registerArray;
    this.address = address;
    this.register = register;
  }

  read(): number {
    return this.registerArray[this.address];
  }

  write(value: number): void {
    this.registerArray[this.address] = value;
  }
}

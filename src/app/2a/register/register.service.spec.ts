import { TestBed } from '@angular/core/testing';

import { RegisterService } from './register.service';
import { RegisterHandlerContext } from './register-handler-context';
import { RegisterHandler } from './register-handler.interface';

class SimpleRegister implements RegisterHandler {
  registered(context) {
    context.write(255);
  }
  read(context: RegisterHandlerContext): number {
    return context.read();
  }
  write(context: RegisterHandlerContext, value: number): void {
    context.write(value);
  }
}

describe('RegisterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    expect(register).toBeTruthy();
  });

  it('should read and write', () => {
    const register: RegisterService = TestBed.get(RegisterService);

    register.write(0, 1);
    register.write(1, 2);

    expect(register.read(0)).toEqual(1);
    expect(register.read(1)).toEqual(2);
    expect(register.read(2)).toEqual(0);

    expect(() => register.read(-1)).toThrowError('register(-1) out of range [0, 7]');
    expect(() => register.read(8)).toThrowError('register(8) out of range [0, 7]');
  });

  it('should register custom register', () => {
    const register: RegisterService = TestBed.get(RegisterService);

    register.registerRegister(2, new SimpleRegister());

    expect(() => register.registerRegister(2, new SimpleRegister()))
      .toThrowError('Can\'t overwrite existing register at (2)');
  });

  it('should read and write from custom register', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    register.registerRegister(2, new SimpleRegister());

    expect(register.read(2)).toEqual(255);

    register.write(0, 1);
    register.write(1, 2);
    register.write(2, 3);
    register.write(3, 4);
    register.write(4, 5);

    expect(register.read(0)).toEqual(1);
    expect(register.read(1)).toEqual(2);
    expect(register.read(2)).toEqual(3);
    expect(register.read(3)).toEqual(4);
    expect(register.read(4)).toEqual(5);

    expect(register.registerArray.slice(0, 5))
      .toEqual(new Uint8Array([ 1, 2, 3, 4, 5 ]));
  });
});

import { Injectable } from '@angular/core';
import { MemoryService } from '../../memory/memory.service';
import { RegisterService } from '../register.service';
import { RegisterHandler } from '../register-handler.interface';
import { RegisterHandlerContext } from '../register-handler-context';

@Injectable({
  providedIn: 'root'
})
export class StackService implements RegisterHandler {

  private context: RegisterHandlerContext;

  private stackbase: number;
  private stacksize = 0;

  constructor(
    private memory: MemoryService,
    private register: RegisterService
  ) {
    this.register.registerRegister(5, this);
  }

  set size(value: number) {
    if (value !== 16 && value !== 32 && value !== 48 && value !== 64 && value !== -1) {
      throw new Error('invalid stack size, valid sizes (16, 32, 48, 64, NOSET)');
    }
    this.stacksize = value;
  }

  get size() {
    return this.stacksize;
  }

  load(address: number) {
    if (address < 0 || address > 0xEF) {
      throw new Error(`stack(${address}) out of range [0, 239]`);
    }
    this.context.write(address);
    this.stackbase = address;
  }

  get address() {
    return this.stackbase;
  }

  push(value: number) {
    const address = this.context.read();
    if (this.stackbase - this.stacksize === address) {
      throw new Error(`stack overflow`);
    }
    this.memory.write(address, value);
    this.context.write(address - 1);
  }

  pop(): number {
    const address = this.context.read();
    if (this.stackbase === address) {
      throw new Error('stack underflow');
    }
    this.context.write(address + 1);
    return this.memory.read(address + 1);
  }

  top(): number {
    const address = this.context.read();
    if (this.stackbase === address) {
      return this.memory.read(address);
    }
    return this.memory.read(address + 1);
  }

  registered(context: RegisterHandlerContext): void {
    this.context = context;
  }

  read(context: RegisterHandlerContext): number {
    return context.read();
  }

  write(context: RegisterHandlerContext, value: number): void {
    throw new Error('can\'t write to stackpointer, use LDSP instruction instead');
  }
}

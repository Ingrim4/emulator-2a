import { RegisterHandler } from '../register-handler.interface';
import { RegisterHandlerContext } from '../register-handler-context';

export class InternalRegister implements RegisterHandler {

  public static readonly INSTANCE = new InternalRegister();

  registered(context: RegisterHandlerContext): void {
  }

  read(context: RegisterHandlerContext): number {
    throw new Error('can\'t read from internal register');
  }

  write(context: RegisterHandlerContext, value: number): void {
    throw new Error('can\'t write to internal register');
  }
}

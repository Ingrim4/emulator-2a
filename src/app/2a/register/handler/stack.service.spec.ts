import { TestBed } from '@angular/core/testing';

import { StackService } from './stack.service';
import { RegisterService } from '../register.service';

describe('StackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const stack: StackService = TestBed.get(StackService);
    expect(stack).toBeTruthy();
  });

  it('should STACKSIZE and LDSP', () => {
    const stack: StackService = TestBed.get(StackService);
    const register: RegisterService = TestBed.get(RegisterService);

    stack.size = 16;
    stack.load(0xEF);

    expect(stack.size).toEqual(16);
    expect(stack.address).toEqual(0xEF);

    expect(register.read(5)).toEqual(0xEF);
    expect(() => register.write(5, 0)).toThrowError('can\'t write to stackpointer, use LDSP instruction instead');

    expect(() => stack.size = 0).toThrowError('invalid stack size, valid sizes (16, 32, 48, 64, NOSET)');
    expect(() => stack.load(0xFF)).toThrowError('stack(255) out of range [0, 239]');
  });

  it('should PUSH and POP', () => {
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    for (let i = 0; i < 16; i++) {
      stack.push(i + 1);
    }

    expect(() => stack.push(0)).toThrowError('stack overflow');

    for (let i = 0; i < 16; i++) {
      expect(stack.pop()).toEqual(16 - i);
    }

    expect(() => stack.pop()).toThrowError('stack underflow');
  });

  it('should return TOP', () => {
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    expect(stack.top()).toEqual(0);
    for (let i = 0; i < 16; i++) {
      stack.push(i + 1);
      expect(stack.top()).toEqual(i + 1);
    }

    for (let i = 0; i < 16; i++) {
      expect(stack.top()).toEqual(16 - i);
      expect(stack.pop()).toEqual(16 - i);
    }
  });
});

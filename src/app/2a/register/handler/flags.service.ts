import { Injectable } from '@angular/core';
import { RegisterService } from '../register.service';
import { RegisterHandler } from '../register-handler.interface';
import { RegisterHandlerContext } from '../register-handler-context';

enum Flag {
  CARRY = 0,
  ZERO = 1,
  NEGATIVE = 2,
  INTERRUPT_ENABLE = 3
}

@Injectable({
  providedIn: 'root'
})
export class FlagsService implements RegisterHandler {

  private context: RegisterHandlerContext;

  constructor(
    private register: RegisterService
  ) {
    this.register.registerRegister(4, this);
  }

  private getFlag(bit: Flag): boolean {
    return (this.context.read() & (1 << bit)) !== 0;
  }

  private setFlag(bit: Flag, value: boolean): void {
    let flags = this.context.read();
    if (value) {
      flags |= (1 << bit);
    } else {
      flags &= ~(1 << bit);
    }
    this.context.write(flags);
  }

  get value() {
    return this.context.read();
  }

  set value(value: number) {
    if (value > 15) {
      throw new Error(`flagsRegisterValue(${value}) out of range [0, 15]`);
    }
    this.context.write(value);
  }

  get carry(): boolean {
    return this.getFlag(Flag.CARRY);
  }

  set carry(value: boolean) {
    this.setFlag(Flag.CARRY, value);
  }

  get zero(): boolean {
    return this.getFlag(Flag.ZERO);
  }

  set zero(value: boolean) {
    this.setFlag(Flag.ZERO, value);
  }

  get negative(): boolean {
    return this.getFlag(Flag.NEGATIVE);
  }

  set negative(value: boolean) {
    this.setFlag(Flag.NEGATIVE, value);
  }

  get interruptEnable(): boolean {
    return this.getFlag(Flag.INTERRUPT_ENABLE);
  }

  set interruptEnable(value: boolean) {
    this.setFlag(Flag.INTERRUPT_ENABLE, value);
  }

  registered(context: RegisterHandlerContext): void {
    this.context = context;
  }

  read(context: RegisterHandlerContext): number {
    return context.read();
  }

  write(context: RegisterHandlerContext, value: number): void {
    throw new Error('can\'t write to flags register');
  }
}

import { TestBed } from '@angular/core/testing';
import { InternalRegister } from './internal-register';
import { RegisterService } from '../register.service';

describe('InternalRegister', () => {
  it('should create an instance', () => {
    expect(InternalRegister.INSTANCE).toBeTruthy();
  });

  it('should not read or write', () => {
    const register: RegisterService = TestBed.get(RegisterService);

    expect(() => register.read(6)).toThrowError('can\'t read from internal register');
    expect(() => register.read(7)).toThrowError('can\'t read from internal register');

    expect(() => register.write(6, 0)).toThrowError('can\'t write to internal register');
    expect(() => register.write(7, 0)).toThrowError('can\'t write to internal register');
  });
});

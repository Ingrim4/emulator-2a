import { TestBed } from '@angular/core/testing';

import { FlagsService } from './flags.service';
import { RegisterService } from '../register.service';

describe('FlagsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const flags: FlagsService = TestBed.get(FlagsService);
    expect(flags).toBeTruthy();
  });

  it('should get/set value', () => {
    const flags: FlagsService = TestBed.get(FlagsService);

    expect(flags.value).toEqual(0);

    flags.value = 0b1110;
    expect(flags.value).toEqual(0b1110);

    expect(() => flags.value = 16).toThrowError('flagsRegisterValue(16) out of range [0, 15]');
  });

  it('should get/set carry', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    expect(flags.carry).toEqual(false);

    flags.carry = true;
    expect(flags.carry).toEqual(true);
    expect(register.read(4)).toEqual(0b0001);

    flags.carry = false;
    expect(flags.carry).toEqual(false);
  });

  it('should get/set zero', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    expect(flags.zero).toEqual(false);

    flags.zero = true;
    expect(flags.zero).toEqual(true);
    expect(register.read(4)).toEqual(0b0010);

    flags.zero = false;
    expect(flags.zero).toEqual(false);
  });

  it('should get/set negative', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    expect(flags.negative).toEqual(false);

    flags.negative = true;
    expect(flags.negative).toEqual(true);
    expect(register.read(4)).toEqual(0b0100);

    flags.negative = false;
    expect(flags.negative).toEqual(false);
  });

  it('should get/set interruptEnable', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    expect(flags.interruptEnable).toEqual(false);

    flags.interruptEnable = true;
    expect(flags.interruptEnable).toEqual(true);
    expect(register.read(4)).toEqual(0b1000);

    flags.interruptEnable = false;
    expect(flags.interruptEnable).toEqual(false);
  });

  it('should read and not write', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    expect(register.read(4)).toEqual(0);
    flags.carry = true;
    flags.negative = true;
    expect(register.read(4)).toEqual(0b0101);

    expect(() => register.write(4, 0)).toThrowError('can\'t write to flags register');
  });
});

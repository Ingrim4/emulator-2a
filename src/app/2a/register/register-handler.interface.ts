import { RegisterHandlerContext } from './register-handler-context';

export interface RegisterHandler {
  registered(context: RegisterHandlerContext): void;
  read(context: RegisterHandlerContext): number;
  write(context: RegisterHandlerContext, value: number): void;
}

import { Injectable, EventEmitter, Output } from '@angular/core';

import { InstructionArithmeticService } from './instruction/instruction-arithmetic.service';
import { InstructionBranchService } from './instruction/instruction-branch.service';
import { InstructionDataService } from './instruction/instruction-data.service';
import { InstructionCompareService } from './instruction/instruction-compare.service';
import { InstructionLogicalService } from './instruction/instruction-logical.service';
import { InstructionShiftService } from './instruction/instruction-shift.service';
import { InstructionSystemService } from './instruction/instruction-system.service';

import { Instruction } from './instruction/instruction.decorator';
import { ParameterizedInstruction } from './instruction/instruction.interface';

import { RegisterService } from './register/register.service';
import { MemoryService } from './memory/memory.service';
import { StackService } from './register/handler/stack.service';
import { ExecuteEvent } from './execute-event.interface';

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  @Output()
  execute = new EventEmitter<ExecuteEvent>();

  private instructions: ParameterizedInstruction[];

  private readonly instructionServices: any[] = [];

  private clockRunning = false;
  private clockTask: number;

  constructor(
    private arithmetic: InstructionArithmeticService,
    private branch: InstructionBranchService,
    private compare: InstructionCompareService,
    private data: InstructionDataService,
    private logical: InstructionLogicalService,
    private shift: InstructionShiftService,
    private system: InstructionSystemService,
    private register: RegisterService,
    private memory: MemoryService,
    private stack: StackService,
  ) {
    this.instructionServices.push(arithmetic);
    this.instructionServices.push(branch);
    this.instructionServices.push(compare);
    this.instructionServices.push(data);
    this.instructionServices.push(logical);
    this.instructionServices.push(shift);
    this.instructionServices.push(system);

    this.stack.size = 16;

    this.tick = this.tick.bind(this);
  }

  reset() {
    const memory = this.memory.memory;
    for (let i = 0; i < memory.length; i++) {
      memory[i] = 0;
    }
    const register = this.register.registerArray;
    for (let i = 0; i < register.length; i++) {
      register[i] = 0;
    }
  }

  load(instructions: ParameterizedInstruction[], origin: number = 0) {
    this.reset();
    this.instructions = instructions;
    this.system.running = true;
    this.register.write(3, origin);
  }

  toggleClock() {
    if (this.clockRunning) {
      cancelAnimationFrame(this.clockTask);
      this.clockRunning = false;
    } else {
      this.clockTask = requestAnimationFrame(this.tick);
      this.clockRunning = true;
    }
  }

  private tick() {
    if (this.step()) {
      this.clockTask = requestAnimationFrame(this.tick);
    } else {
      this.clockRunning = false;
    }
  }

  step(): boolean {
    if (!this.system.running) {
      return false;
    }

    let programCounter = this.register.read(3);
    // if programCounter points at nothing jump to start
    if (programCounter >= this.instructions.length) {
      programCounter = 0;
    }

    const instruction = this.instructions[programCounter];
    this.register.write(3, programCounter + 1);

    try {
      this.executeInstruction(instruction);
    } catch (error) {
      this.system.running = false;

      this.execute.emit({ programCounter, error });
      return false;
    }

    this.execute.emit({ programCounter });

    return true;
  }

  private executeInstruction(instruction: ParameterizedInstruction) {
    const targetService = Instruction.instructionToService[instruction.name];
    for (const service of this.instructionServices) {
      if (Object.getPrototypeOf(service).constructor === targetService) {
        service[instruction.name].call(service, instruction.dst, instruction.src);
        return;
      }
    }
    throw new Error(`unknown instruction: ${instruction.name}`);
  }
}

import { TestBed, flush, fakeAsync } from '@angular/core/testing';

import { CoreService } from './core.service';
import { ParameterizedInstruction } from './instruction/instruction.interface';
import { IoAddress, IoAddressType } from './io/io-address';
import { MemoryService } from './memory/memory.service';
import { RegisterService } from './register/register.service';
import { InstructionSystemService } from './instruction/instruction-system.service';

describe('CoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const instructions: ParameterizedInstruction[] = [
    {
      name: 'MOV',
      dst: new IoAddress(IoAddressType.REGISTER, 0),
      src: new IoAddress(IoAddressType.CONST, 0xFF)
    },
    {
      name: 'MOV',
      dst: new IoAddress(IoAddressType.ADDRESS, 0x10),
      src: new IoAddress(IoAddressType.CONST, 0xFF)
    },
    {
      name: 'STOP'
    }
  ];

  const invalidInstructions: ParameterizedInstruction[] = [
    {
      name: 'BAR'
    },
    {
      name: 'JR',
      dst: new IoAddress(IoAddressType.CONST, 0x0)
    }
  ];

  it('should be created', () => {
    const core: CoreService = TestBed.get(CoreService);
    expect(core).toBeTruthy();
  });

  it('should reset 2a', () => {
    const core: CoreService = TestBed.get(CoreService);
    const memory: MemoryService = TestBed.get(MemoryService);
    const register: RegisterService = TestBed.get(RegisterService);

    memory.write(0, 0xFF);
    register.write(0, 0xFF);

    core.reset();

    expect(memory.read(0)).toEqual(0x00);
    expect(register.read(0)).toEqual(0x00);
  });

  it('shouldn\'t execute invalid instructions', () => {
    const core: CoreService = TestBed.get(CoreService);
    const spy = spyOn(core.execute, 'emit');

    core.load(invalidInstructions);
    core.step();

    expect(core.execute.emit).toHaveBeenCalledTimes(1);

    const args = spy.calls.argsFor(0);
    expect(args[0].programCounter).toEqual(0);
    expect(args[0].error.message).toEqual('unknown instruction: BAR');
  });

  it('should execute valid instructions', () => {
    const core: CoreService = TestBed.get(CoreService);
    const memory: MemoryService = TestBed.get(MemoryService);
    const register: RegisterService = TestBed.get(RegisterService);
    const system: InstructionSystemService = TestBed.get(InstructionSystemService);
    const spy = spyOn(core.execute, 'emit');

    core.load(instructions);
    core.step();
    core.step();
    core.step();

    expect(core.execute.emit).toHaveBeenCalledTimes(3);

    let args = spy.calls.argsFor(0);
    expect(args[0].programCounter).toEqual(0);

    args = spy.calls.argsFor(1);
    expect(args[0].programCounter).toEqual(1);

    args = spy.calls.argsFor(2);
    expect(args[0].programCounter).toEqual(2);

    expect(memory.read(0x10)).toEqual(0xFF);
    expect(register.read(0)).toEqual(0xFF);
    expect(system.running).toEqual(false);
  });

  it('should tick', () => {
    const core: CoreService = TestBed.get(CoreService);
    const memory: MemoryService = TestBed.get(MemoryService);
    const register: RegisterService = TestBed.get(RegisterService);
    const system: InstructionSystemService = TestBed.get(InstructionSystemService);

    window.requestAnimationFrame = (func: FrameRequestCallback) => {
      func(-1);
      return -1;
    };

    core.load(instructions);
    core.toggleClock();

    expect(memory.read(0x10)).toEqual(0xFF);
    expect(register.read(0)).toEqual(0xFF);
    expect(system.running).toEqual(false);
  });

  it('should jump to 0 on invalid programCounter value', () => {
    const core: CoreService = TestBed.get(CoreService);
    const register: RegisterService = TestBed.get(RegisterService);
    const spy = spyOn(core.execute, 'emit');

    core.load(invalidInstructions);
    register.write(3, 1);
    core.step();
    core.step();

    expect(core.execute.emit).toHaveBeenCalledTimes(2);

    const args = spy.calls.argsFor(1);
    expect(args[0].programCounter).toEqual(0);
  });

  it('should toggle the clock', () => {
    const core: CoreService = TestBed.get(CoreService);

    core.load(instructions);

    core.toggleClock();
    core.toggleClock();
  });
});

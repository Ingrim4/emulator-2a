import { TestBed } from '@angular/core/testing';

import { InstructionShiftService } from './instruction-shift.service';
import { RegisterService } from '../register/register.service';
import { FlagsService } from '../register/handler/flags.service';
import { IoAddress, IoAddressType } from '../io/io-address';


describe('InstructionShiftService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const address: IoAddress = new IoAddress(IoAddressType.REGISTER, 0);

  it('should be created', () => {
    const shift: InstructionShiftService = TestBed.get(InstructionShiftService);
    expect(shift).toBeTruthy();
  });

  it('LSR instruction', () => {
    const shift: InstructionShiftService = TestBed.get(InstructionShiftService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255 >> 1 = 127
    register.write(address.value, 0xFF);
    shift.LSR(address);
    expect(register.read(address.value)).toEqual(0x7F);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 254 >> 1 = 127
    register.write(address.value, 0xFE);
    shift.LSR(address);
    expect(register.read(address.value)).toEqual(0x7F);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 1 >> 1 = 0
    register.write(address.value, 0x01);
    shift.LSR(address);
    expect(register.read(address.value)).toEqual(0x00);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);
  });

  it('ASR instruction', () => {
    const shift: InstructionShiftService = TestBed.get(InstructionShiftService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255 >> 1 = 191
    register.write(address.value, 0xFF);
    shift.ASR(address);
    expect(register.read(address.value)).toEqual(0xBF);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 254 >> 1 = 191
    register.write(address.value, 0xFE);
    shift.ASR(address);
    expect(register.read(address.value)).toEqual(0xBF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 1 >> 1 = 0
    register.write(address.value, 0x01);
    shift.ASR(address);
    expect(register.read(address.value)).toEqual(0x00);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);
  });

  it('LSL instruction', () => {
    const shift: InstructionShiftService = TestBed.get(InstructionShiftService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255 << 1 = 254
    register.write(address.value, 0xFF);
    shift.LSL(address);
    expect(register.read(address.value)).toEqual(0xFE);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 127 << 1 = 254
    register.write(address.value, 0x7F);
    shift.LSL(address);
    expect(register.read(address.value)).toEqual(0xFE);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 128 << 1 = 0
    register.write(address.value, 0x80);
    shift.LSL(address);
    expect(register.read(address.value)).toEqual(0x00);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);
  });

  it('RRC instruction', () => {
    const shift: InstructionShiftService = TestBed.get(InstructionShiftService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255 >> 1 = 127
    register.write(address.value, 0xFF);
    shift.RRC(address);
    expect(register.read(address.value)).toEqual(0x7F);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test (255 >> 1) | 128 = 255
    register.write(address.value, 0xFF);
    shift.RRC(address);
    expect(register.read(address.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test (2 >> 1) | 128 = 129
    register.write(address.value, 0x02);
    shift.RRC(address);
    expect(register.read(address.value)).toEqual(0x81);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 1 >> 1 = 0
    register.write(address.value, 0x01);
    shift.RRC(address);
    expect(register.read(address.value)).toEqual(0x00);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);
  });

  it('RLC instruction', () => {
    const shift: InstructionShiftService = TestBed.get(InstructionShiftService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255 << 1 = 254
    register.write(address.value, 0xFF);
    shift.RLC(address);
    expect(register.read(address.value)).toEqual(0xFE);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test (255 << 1) | 1 = 255
    register.write(address.value, 0xFF);
    shift.RLC(address);
    expect(register.read(address.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test (2 << 1) | 1 = 5
    register.write(address.value, 0x02);
    shift.RLC(address);
    expect(register.read(address.value)).toEqual(0x05);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 128 << 1 = 0
    register.write(address.value, 0x80);
    shift.RLC(address);
    expect(register.read(address.value)).toEqual(0x00);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);
  });
});

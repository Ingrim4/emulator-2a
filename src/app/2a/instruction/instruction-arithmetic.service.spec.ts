import { TestBed } from '@angular/core/testing';

import { InstructionArithmeticService } from './instruction-arithmetic.service';
import { RegisterService } from '../register/register.service';
import { FlagsService } from '../register/handler/flags.service';
import { IoAddress, IoAddressType } from '../io/io-address';


describe('InstructionArithmeticService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const addressSrc: IoAddress = new IoAddress(IoAddressType.REGISTER, 0);
  const addressDst: IoAddress = new IoAddress(IoAddressType.REGISTER, 1);

  it('should be created', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    expect(arithmetic).toBeTruthy();
  });

  it('CLR instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.carry = true;
    flags.zero = true;
    flags.negative = false;

    register.write(addressDst.value, 0xFF);
    arithmetic.CLR(addressDst);

    expect(register.read(addressDst.value)).toEqual(0);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);
  });

  it('ADD instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test -1 + -1 = -2
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0xFF);
    arithmetic.ADD(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFE);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test -127 + 127 = 0
    register.write(addressDst.value, 0x81);
    register.write(addressSrc.value, 0x7F);
    arithmetic.ADD(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test -1 + 0 = -1
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0);
    arithmetic.ADD(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 127 + 2 = -127
    register.write(addressDst.value, 0x7F);
    register.write(addressSrc.value, 0x02);
    arithmetic.ADD(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x81);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 127 + 127 = -2
    register.write(addressDst.value, 0x7F);
    register.write(addressSrc.value, 0x7F);
    arithmetic.ADD(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFE);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);
  });

  it('ADC instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test -1 + -1 + 0 = -2
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0xFF);
    arithmetic.ADC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFE);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test -127 + 127 + 1 = 1
    register.write(addressDst.value, 0x81);
    register.write(addressSrc.value, 0x7F);
    arithmetic.ADC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x01);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test -1 + 0 + 1 = 0
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0x00);
    arithmetic.ADC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 127 + 2 + 1 = -126
    register.write(addressDst.value, 0x7F);
    register.write(addressSrc.value, 0x02);
    arithmetic.ADC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x82);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 127 + 127 + 0 = -2
    register.write(addressDst.value, 0x7F);
    register.write(addressSrc.value, 0x7F);
    arithmetic.ADC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFE);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);
  });

  it('SUB instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test -1 - -1 = 0
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0xFF);
    arithmetic.SUB(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test -127 - 127 = 0
    register.write(addressDst.value, 0x81);
    register.write(addressSrc.value, 0x7F);
    arithmetic.SUB(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x02);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 0 - -128 = 1
    register.write(addressDst.value, 0x00);
    register.write(addressSrc.value, 0xFF);
    arithmetic.SUB(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x01);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 2 - 127 = -125
    register.write(addressDst.value, 0x02);
    register.write(addressSrc.value, 0x7F);
    arithmetic.SUB(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x83);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 127 - 127 = 0
    register.write(addressDst.value, 0x7F);
    register.write(addressSrc.value, 0x7F);
    arithmetic.SUB(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);
  });

  it('MUL instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test -1 * -1 = 1
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0xFF);
    arithmetic.MUL(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x01);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test -127 * 127 = -1
    register.write(addressDst.value, 0x81);
    register.write(addressSrc.value, 0x7F);
    arithmetic.MUL(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 0 * -128 = 0
    register.write(addressDst.value, 0x00);
    register.write(addressSrc.value, 0xFF);
    arithmetic.MUL(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 2 * 127 = -2
    register.write(addressDst.value, 0x02);
    register.write(addressSrc.value, 0x7F);
    arithmetic.MUL(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFE);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 127 * 127 = 1
    register.write(addressDst.value, 0x7F);
    register.write(addressSrc.value, 0x7F);
    arithmetic.MUL(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x01);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);
  });

  it('DIV instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test -1 / -1 = 1
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0xFF);
    arithmetic.DIV(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x01);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test -127 / 127 = 1
    register.write(addressDst.value, 0x81);
    register.write(addressSrc.value, 0x7F);
    arithmetic.DIV(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x01);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 0 / -128 = 0
    register.write(addressDst.value, 0x00);
    register.write(addressSrc.value, 0xFF);
    arithmetic.DIV(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 2 / 127 = 0
    register.write(addressDst.value, 0x02);
    register.write(addressSrc.value, 0x7F);
    arithmetic.DIV(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 2 / 0 = 0
    register.write(addressDst.value, 0x02);
    register.write(addressSrc.value, 0x00);
    arithmetic.DIV(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 126 / 2 = 63
    register.write(addressDst.value, 0x7E);
    register.write(addressSrc.value, 0x02);
    arithmetic.DIV(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x3F);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);
  });

  it('INC instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    register.write(addressDst.value, 0);
    for (let i = 0; i < 0xFF; i++) {
      arithmetic.INC(addressDst);
      expect(register.read(addressDst.value)).toEqual(i + 1);
      expect(flags.carry).toEqual(false);
      expect(flags.zero).toEqual(false);
      expect(flags.negative).toEqual(i >= 127);
    }

    arithmetic.INC(addressDst);
    expect(register.read(addressDst.value)).toEqual(0);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);
  });

  it('DEC instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    register.write(addressDst.value, 0xFF);
    for (let i = 0; i < 0xFF; i++) {
      arithmetic.DEC(addressDst);
      expect(register.read(addressDst.value)).toEqual(0xFF - (i + 1));
      expect(flags.carry).toEqual(false);
      expect(flags.zero).toEqual(i === 0xFE);
      expect(flags.negative).toEqual(i < 127);
    }

    arithmetic.DEC(addressDst);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);
  });

  it('NEG instruction', () => {
    const arithmetic: InstructionArithmeticService = TestBed.get(InstructionArithmeticService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // ~0 + 1 = 0
    register.write(addressDst.value, 0x00);
    arithmetic.NEG(addressDst);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // ~72 + 1 = -72
    register.write(addressDst.value, 0x48);
    arithmetic.NEG(addressDst);
    expect(register.read(addressDst.value)).toEqual(0xB8);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // ~(-44) + 1 = 44
    register.write(addressDst.value, 0xD4);
    arithmetic.NEG(addressDst);
    expect(register.read(addressDst.value)).toEqual(0x2C);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);
  });
});

import { Injectable } from '@angular/core';

import { IoService } from '../io/io.service';
import { IoAddress } from '../io/io-address';
import { FlagsService } from '../register/handler/flags.service';
import { Instruction, ParameterType } from './instruction.decorator';

@Injectable({
  providedIn: 'root'
})
export class InstructionArithmeticService {

  constructor(
    private io: IoService,
    private flags: FlagsService
  ) { }

  private updateFlags(value: number): void {
    this.flags.carry = (value & 0x100) !== 0;
    this.flags.zero = (value & 0xFF) === 0;
    this.flags.negative = (value & 0x80) !== 0;
  }

  @Instruction(ParameterType.REGISTER)
  CLR(dst: IoAddress): void {
    this.io.write(dst, 0);
  }

  @Instruction(ParameterType.REGISTER, ParameterType.REGISTER)
  ADD(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value += this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER, ParameterType.REGISTER)
  ADC(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value += this.io.read(src);
    value += this.flags.carry ? 1 : 0;

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER, ParameterType.REGISTER)
  SUB(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value -= this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER, ParameterType.REGISTER)
  MUL(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value *= this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER, ParameterType.REGISTER)
  DIV(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value /= this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER)
  INC(dst: IoAddress) {
    let value = this.io.read(dst);
    value += 1;

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction([ParameterType.REGISTER, ParameterType.ADDRESS])
  DEC(dst: IoAddress) {
    let value = this.io.read(dst);
    value -= 1;

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER)
  NEG(dst: IoAddress) {
    let value = this.io.read(dst);
    value = (~value & 0xFF) + 1;

    this.updateFlags(value);
    this.io.write(dst, value);
  }
}

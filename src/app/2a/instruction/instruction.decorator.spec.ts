import { IoAddress, IoAddressType } from '../io/io-address';
import { Instruction, ParameterType } from './instruction.decorator';

describe('Instruction', () => {

  const addressRegister = new IoAddress(IoAddressType.REGISTER, 0);
  const addressAddress = new IoAddress(IoAddressType.ADDRESS, 0);
  const addressConst = new IoAddress(IoAddressType.CONST, 0);

  it('should modify function', () => {
    class Test {
      @Instruction(ParameterType.ALL, ParameterType.CONST)
      test(dst?: IoAddress, src?: IoAddress) { }

      @Instruction(ParameterType.ALL, ParameterType.ADDRESS)
      testAddress(dst?: IoAddress, src?: IoAddress) { }

      @Instruction([ParameterType.CONST, ParameterType.REGISTER], [ParameterType.CONST, ParameterType.REGISTER])
      testMulti(dst?: IoAddress, src?: IoAddress) { }
    }

    const test = new Test();
    expect(() => test.test()).toThrowError('missing argument dst');
    expect(() => test.test(addressRegister, addressRegister))
      .toThrowError('invalid argument src: REGISTER !== [CONST]');

    expect(() => test.testAddress(addressRegister, addressRegister))
      .toThrowError('invalid argument src: REGISTER !== [ADDRESS]');

    test.testMulti(addressConst, addressConst);
    expect(() => test.testMulti(addressAddress, addressConst))
      .toThrowError('invalid argument dst: ADDRESS !== [CONST,REGISTER]');
  });
});

import { TestBed } from '@angular/core/testing';

import { InstructionCompareService } from './instruction-compare.service';
import { RegisterService } from '../register/register.service';
import { FlagsService } from '../register/handler/flags.service';
import { IoAddress, IoAddressType } from '../io/io-address';

describe('InstructionCompareService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const addressSrc: IoAddress = new IoAddress(IoAddressType.REGISTER, 0);
  const addressDst: IoAddress = new IoAddress(IoAddressType.REGISTER, 1);

  it('should be created', () => {
    const compare: InstructionCompareService = TestBed.get(InstructionCompareService);
    expect(compare).toBeTruthy();
  });

  it('TST instruction', () => {
    const compare: InstructionCompareService = TestBed.get(InstructionCompareService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255
    register.write(addressDst.value, 0xFF);
    compare.TST(addressDst);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 0
    register.write(addressDst.value, 0x00);
    compare.TST(addressDst);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 64
    register.write(addressDst.value, 0x40);
    compare.TST(addressDst);
    expect(register.read(addressDst.value)).toEqual(0x40);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);
  });

  it('CMP instruction', () => {
    const compare: InstructionCompareService = TestBed.get(InstructionCompareService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 32 === 32
    register.write(addressDst.value, 0x20);
    register.write(addressSrc.value, 0x20);
    compare.CMP(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x20);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 32 > 16
    register.write(addressDst.value, 0x20);
    register.write(addressSrc.value, 0x10);
    compare.CMP(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x20);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 64 < 32
    register.write(addressDst.value, 0x20);
    register.write(addressSrc.value, 0x40);
    compare.CMP(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x20);
    expect(flags.carry).toEqual(true);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);
  });

  it('BITT instruction', () => {
    const compare: InstructionCompareService = TestBed.get(InstructionCompareService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 32 & 32 !== 0
    register.write(addressDst.value, 0x20);
    register.write(addressSrc.value, 0x20);
    compare.BITT(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x20);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 32 & 16 !== 0
    register.write(addressDst.value, 0x20);
    register.write(addressSrc.value, 0x10);
    compare.BITT(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x20);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 31 & 16 !== 0
    register.write(addressDst.value, 0x1F);
    register.write(addressSrc.value, 0x10);
    compare.BITT(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x1F);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);
  });
});

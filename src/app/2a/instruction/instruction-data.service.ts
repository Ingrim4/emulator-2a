import { Injectable } from '@angular/core';

import { IoService } from '../io/io.service';
import { IoAddress } from '../io/io-address';
import { FlagsService } from '../register/handler/flags.service';
import { StackService } from '../register/handler/stack.service';
import { Instruction, ParameterType } from './instruction.decorator';

@Injectable({
  providedIn: 'root'
})
export class InstructionDataService {

  constructor(
    private io: IoService,
    private stack: StackService,
    private flags: FlagsService
  ) { }

  @Instruction(ParameterType.ALL, ParameterType.ALL)
  MOV(dst: IoAddress, src: IoAddress) {
    this.io.write(dst, this.io.read(src));
  }

  @Instruction(ParameterType.REGISTER, [ParameterType.CONST, ParameterType.ADDRESS])
  LD(dst: IoAddress, src: IoAddress) {
    this.io.write(dst, this.io.read(src));
  }

  @Instruction(ParameterType.ADDRESS, ParameterType.REGISTER)
  ST(dst: IoAddress, src: IoAddress) {
    this.io.write(dst, this.io.read(src));
  }

  @Instruction(ParameterType.REGISTER)
  PUSH(dst: IoAddress) {
    this.stack.push(this.io.read(dst));
  }

  @Instruction(ParameterType.REGISTER)
  POP(dst: IoAddress) {
    this.io.write(dst, this.stack.pop());
  }

  @Instruction()
  PUSHF() {
    this.stack.push(this.flags.value);
  }

  @Instruction()
  POPF() {
    this.flags.value = this.stack.pop();
  }

  @Instruction(ParameterType.ALL)
  LDSP(dst: IoAddress) {
    this.stack.load(this.io.read(dst));
  }

  @Instruction(ParameterType.ALL)
  LDFR(dst: IoAddress) {
    this.flags.value = this.io.read(dst);
  }
}

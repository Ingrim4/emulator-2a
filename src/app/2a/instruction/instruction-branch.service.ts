import { Injectable } from '@angular/core';

import { IoService } from '../io/io.service';
import { IoAddress } from '../io/io-address';
import { RegisterService } from '../register/register.service';
import { StackService } from '../register/handler/stack.service';
import { FlagsService } from '../register/handler/flags.service';
import { Instruction, ParameterType } from './instruction.decorator';

@Injectable({
  providedIn: 'root'
})
export class InstructionBranchService {

  constructor(
    private io: IoService,
    private register: RegisterService,
    private stack: StackService,
    private flags: FlagsService
  ) { }

  @Instruction(ParameterType.CONST)
  JMP(dst: IoAddress) {
    this.register.write(3, this.io.read(dst));
  }

  @Instruction(ParameterType.CONST)
  JCS(dst: IoAddress) {
    if (this.flags.carry) {
      this.register.write(3, this.register.read(3) + this.io.read(dst));
    }
  }

  @Instruction(ParameterType.CONST)
  JCC(dst: IoAddress) {
    if (!this.flags.carry) {
      this.register.write(3, this.register.read(3) + this.io.read(dst));
    }
  }

  @Instruction(ParameterType.CONST)
  JZS(dst: IoAddress) {
    if (this.flags.zero) {
      this.register.write(3, this.register.read(3) + this.io.read(dst));
    }
  }

  @Instruction(ParameterType.CONST)
  JZC(dst: IoAddress) {
    if (!this.flags.zero) {
      this.register.write(3, this.register.read(3) + this.io.read(dst));
    }
  }

  @Instruction(ParameterType.CONST)
  JNS(dst: IoAddress) {
    if (this.flags.negative) {
      this.register.write(3, this.register.read(3) + this.io.read(dst));
    }
  }

  @Instruction(ParameterType.CONST)
  JNC(dst: IoAddress) {
    if (!this.flags.negative) {
      this.register.write(3, this.register.read(3) + this.io.read(dst));
    }
  }

  @Instruction(ParameterType.CONST)
  JR(dst: IoAddress) {
    this.register.write(3, this.register.read(3) + this.io.read(dst));
  }

  @Instruction(ParameterType.CONST)
  CALL(dst: IoAddress) {
    this.stack.push(this.register.read(3));
    this.register.write(3, this.io.read(dst));
  }

  @Instruction()
  RET() {
    this.register.write(3, this.stack.pop());
  }

  @Instruction()
  RETI() {
    this.flags.value = this.stack.pop();
    this.flags.interruptEnable = true;
    this.RET();
  }
}

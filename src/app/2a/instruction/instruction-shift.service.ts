import { Injectable } from '@angular/core';

import { IoService } from '../io/io.service';
import { IoAddress } from '../io/io-address';
import { FlagsService } from '../register/handler/flags.service';
import { Instruction, ParameterType } from './instruction.decorator';

@Injectable({
  providedIn: 'root'
})
export class InstructionShiftService {

  constructor(
    private io: IoService,
    private flags: FlagsService
  ) { }

  @Instruction(ParameterType.REGISTER)
  LSR(dst: IoAddress): void {
    let value = this.io.read(dst);
    this.flags.carry = (value & 0x01) !== 0;

    value >>= 1;
    this.flags.zero = (value & 0xFF) === 0;
    this.flags.negative = (value & 0x80) !== 0;

    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER)
  ASR(dst: IoAddress): void {
    let value = this.io.read(dst);
    this.flags.carry = (value & 0x01) !== 0;

    value = (value & 0x80) | ((value & 0x7F) >> 1);
    this.flags.zero = (value & 0xFF) === 0;
    this.flags.negative = (value & 0x80) !== 0;

    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER)
  LSL(dst: IoAddress): void {
    let value = this.io.read(dst);
    this.flags.carry = (value & 0x80) !== 0;

    value <<= 1;
    this.flags.zero = (value & 0xFF) === 0;
    this.flags.negative = (value & 0x80) !== 0;

    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER)
  RRC(dst: IoAddress): void {
    let value = this.io.read(dst);
    const carryIn = (this.flags.carry ? 0x80 : 0x00);
    this.flags.carry = (value & 0x01) !== 0;

    value = carryIn | (value >> 1);
    this.flags.zero = (value & 0xFF) === 0;
    this.flags.negative = (value & 0x80) !== 0;

    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER)
  RLC(dst: IoAddress): void {
    let value = this.io.read(dst);
    const carryIn = (this.flags.carry ? 0x01 : 0x00);
    this.flags.carry = (value & 0x80) !== 0;

    value = carryIn | (value << 1);
    this.flags.zero = (value & 0xFF) === 0;
    this.flags.negative = (value & 0x80) !== 0;

    this.io.write(dst, value);
  }
}

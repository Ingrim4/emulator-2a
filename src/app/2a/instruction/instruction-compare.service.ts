import { Injectable } from '@angular/core';

import { IoService } from '../io/io.service';
import { IoAddress } from '../io/io-address';
import { FlagsService } from '../register/handler/flags.service';
import { Instruction, ParameterType } from './instruction.decorator';

@Injectable({
  providedIn: 'root'
})
export class InstructionCompareService {

  constructor(
    private io: IoService,
    private flags: FlagsService
  ) { }

  private updateFlags(value: number): void {
    this.flags.carry = (value & 0x100) !== 0;
    this.flags.zero = (value & 0xFF) === 0;
    this.flags.negative = (value & 0x80) !== 0;
  }

  @Instruction(ParameterType.REGISTER)
  TST(src: IoAddress): void {
    this.updateFlags(this.io.read(src));
  }

  @Instruction(ParameterType.ALL, ParameterType.ALL)
  CMP(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value -= this.io.read(src);

    this.updateFlags(value);
  }

  @Instruction(ParameterType.ALL, ParameterType.ALL)
  BITT(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value &= this.io.read(src);

    this.updateFlags(value);
  }
}

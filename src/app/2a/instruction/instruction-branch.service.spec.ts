import { TestBed } from '@angular/core/testing';

import { InstructionBranchService } from './instruction-branch.service';
import { RegisterService } from '../register/register.service';
import { FlagsService } from '../register/handler/flags.service';
import { IoAddress, IoAddressType } from '../io/io-address';
import { StackService } from '../register/handler/stack.service';

describe('InstructionBranchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const address: IoAddress = new IoAddress(IoAddressType.CONST, 0x10);

  it('should be created', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    expect(branch).toBeTruthy();
  });

  it('JMP instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);

    branch.JMP(address);
    expect(register.read(3)).toEqual(0x10);

    branch.JMP(address);
    expect(register.read(3)).toEqual(0x10);
  });

  it('JCS instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.carry = true;
    branch.JCS(address);
    expect(register.read(3)).toEqual(0x10);

    branch.JCS(address);
    expect(register.read(3)).toEqual(0x20);

    flags.carry = false;
    branch.JCS(address);
    expect(register.read(3)).toEqual(0x20);
  });

  it('JCC instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.carry = true;
    branch.JCC(address);
    expect(register.read(3)).toEqual(0x00);

    flags.carry = false;
    branch.JCC(address);
    expect(register.read(3)).toEqual(0x10);

    branch.JCC(address);
    expect(register.read(3)).toEqual(0x20);
  });

  it('JZS instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.zero = true;
    branch.JZS(address);
    expect(register.read(3)).toEqual(0x10);

    branch.JZS(address);
    expect(register.read(3)).toEqual(0x20);

    flags.zero = false;
    branch.JZS(address);
    expect(register.read(3)).toEqual(0x20);
  });

  it('JZC instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.zero = true;
    branch.JZC(address);
    expect(register.read(3)).toEqual(0x00);

    flags.zero = false;
    branch.JZC(address);
    expect(register.read(3)).toEqual(0x10);

    branch.JZC(address);
    expect(register.read(3)).toEqual(0x20);
  });

  it('JNS instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.negative = true;
    branch.JNS(address);
    expect(register.read(3)).toEqual(0x10);

    branch.JNS(address);
    expect(register.read(3)).toEqual(0x20);

    flags.negative = false;
    branch.JNS(address);
    expect(register.read(3)).toEqual(0x20);
  });

  it('JNC instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.negative = true;
    branch.JNC(address);
    expect(register.read(3)).toEqual(0x00);

    flags.negative = false;
    branch.JNC(address);
    expect(register.read(3)).toEqual(0x10);

    branch.JNC(address);
    expect(register.read(3)).toEqual(0x20);
  });

  it('JR instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);

    branch.JR(address);
    expect(register.read(3)).toEqual(0x10);

    branch.JR(address);
    expect(register.read(3)).toEqual(0x20);
  });

  it('CALL instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    branch.CALL(address);
    expect(register.read(3)).toEqual(0x10);
    expect(stack.top()).toEqual(0x00);

    branch.CALL(address);
    expect(register.read(3)).toEqual(0x10);
    expect(stack.top()).toEqual(0x10);

    expect(stack.pop()).toEqual(0x10);
    expect(stack.pop()).toEqual(0x00);
  });

  it('RET instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    branch.CALL(address);
    branch.CALL(address);

    branch.RET();
    expect(stack.top()).toEqual(0x00);
    expect(register.read(3)).toEqual(0x10);

    branch.RET();
    expect(stack.top()).toEqual(0x00);
    expect(register.read(3)).toEqual(0x00);
  });

  it('RETI instruction', () => {
    const branch: InstructionBranchService = TestBed.get(InstructionBranchService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    branch.CALL(address);
    stack.push(0b0101);

    branch.RETI();
    expect(flags.value).toEqual(0b1101);
    expect(stack.top()).toEqual(0x00);
    expect(register.read(3)).toEqual(0x00);
  });
});

import { TestBed } from '@angular/core/testing';

import { InstructionDataService } from './instruction-data.service';
import { RegisterService } from '../register/register.service';
import { IoAddress, IoAddressType } from '../io/io-address';
import { StackService } from '../register/handler/stack.service';
import { FlagsService } from '../register/handler/flags.service';
import { MemoryService } from '../memory/memory.service';


describe('InstructionDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const addressDst: IoAddress = new IoAddress(IoAddressType.REGISTER, 0);
  const addressSrc: IoAddress = new IoAddress(IoAddressType.REGISTER, 1);
  const addressAddress: IoAddress = new IoAddress(IoAddressType.ADDRESS, 0x10);
  const addressConst: IoAddress = new IoAddress(IoAddressType.CONST, 0xFF);

  it('should be created', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    expect(data).toBeTruthy();
  });

  it('MOV instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const register: RegisterService = TestBed.get(RegisterService);

    register.write(addressDst.value, 0x10);
    register.write(addressSrc.value, 0x20);
    data.MOV(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x20);
    expect(register.read(addressSrc.value)).toEqual(0x20);
  });

  it('LD instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const register: RegisterService = TestBed.get(RegisterService);

    register.write(addressDst.value, 0x10);
    data.LD(addressDst, addressConst);
    expect(register.read(addressDst.value)).toEqual(0xFF);
  });


  it('ST instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const memory: MemoryService = TestBed.get(MemoryService);
    const register: RegisterService = TestBed.get(RegisterService);

    memory.write(addressAddress.value, 0x10);
    register.write(addressSrc.value, 0x20);
    data.ST(addressAddress, addressSrc);
    expect(memory.read(addressAddress.value)).toEqual(0x20);
    expect(register.read(addressSrc.value)).toEqual(0x20);
  });


  it('PUSH instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const register: RegisterService = TestBed.get(RegisterService);
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    register.write(addressDst.value, 0x10);
    data.PUSH(addressDst);
    expect(stack.top()).toEqual(0x10);

    register.write(addressDst.value, 0x20);
    data.PUSH(addressDst);
    expect(stack.top()).toEqual(0x20);

    expect(stack.pop()).toEqual(0x20);
    expect(stack.pop()).toEqual(0x10);
  });

  it('POP instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const register: RegisterService = TestBed.get(RegisterService);
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    register.write(addressDst.value, 0x10);
    data.PUSH(addressDst);

    register.write(addressDst.value, 0x20);
    data.PUSH(addressDst);

    data.POP(addressDst);
    expect(register.read(addressDst.value)).toEqual(0x20);

    data.POP(addressDst);
    expect(register.read(addressDst.value)).toEqual(0x10);
  });

  it('PUSHF instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const flags: FlagsService = TestBed.get(FlagsService);
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    flags.value = 0b101;
    data.PUSHF();

    expect(stack.top()).toEqual(0b101);
  });

  it('POPF instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const flags: FlagsService = TestBed.get(FlagsService);
    const stack: StackService = TestBed.get(StackService);

    stack.size = 16;
    stack.load(0xEF);

    flags.value = 0b101;
    data.PUSHF();

    flags.value = 0b110;

    data.POPF();
    expect(flags.value).toEqual(0b101);
  });

  it('LDSP instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const register: RegisterService = TestBed.get(RegisterService);

    register.write(addressDst.value, 0x10);
    data.LDSP(addressDst);

    expect(register.read(5)).toEqual(0x10);
  });

  it('LDFR instruction', () => {
    const data: InstructionDataService = TestBed.get(InstructionDataService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    register.write(addressDst.value, 0b101);
    data.LDFR(addressDst);

    expect(flags.value).toEqual(0b101);
  });
});

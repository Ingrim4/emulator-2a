import { Injectable } from '@angular/core';
import { Instruction } from './instruction.decorator';
import { FlagsService } from '../register/handler/flags.service';

@Injectable({
  providedIn: 'root'
})
export class InstructionSystemService {

  running: boolean;

  constructor(
    private flags: FlagsService
  ) { }

  @Instruction()
  STOP() {
    this.running = false;
  }

  @Instruction()
  NOP() {
    // NOP
  }

  @Instruction()
  EI() {
    this.flags.interruptEnable = true;
  }

  @Instruction()
  DI() {
    this.flags.interruptEnable = false;
  }
}

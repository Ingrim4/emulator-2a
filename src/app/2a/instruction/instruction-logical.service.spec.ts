import { TestBed } from '@angular/core/testing';

import { InstructionLogicalService } from './instruction-logical.service';
import { RegisterService } from '../register/register.service';
import { FlagsService } from '../register/handler/flags.service';
import { IoAddress, IoAddressType } from '../io/io-address';

describe('InstructionLogicalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  const addressDst: IoAddress = new IoAddress(IoAddressType.REGISTER, 0);
  const addressSrc: IoAddress = new IoAddress(IoAddressType.REGISTER, 1);

  it('should be created', () => {
    const logical: InstructionLogicalService = TestBed.get(InstructionLogicalService);
    expect(logical).toBeTruthy();
  });

  it('AND instruction', () => {
    const logical: InstructionLogicalService = TestBed.get(InstructionLogicalService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255 & 255 = 255
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0xFF);
    logical.AND(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 16 & 32 = 0
    register.write(addressDst.value, 0x10);
    register.write(addressSrc.value, 0x20);
    logical.AND(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 170 & 85 = 0
    register.write(addressDst.value, 0xAA);
    register.write(addressSrc.value, 0x55);
    logical.AND(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 15 & 170 = 10
    register.write(addressDst.value, 0x0F);
    register.write(addressSrc.value, 0xAA);
    logical.AND(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x0A);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);
  });

  it('OR instruction', () => {
    const logical: InstructionLogicalService = TestBed.get(InstructionLogicalService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255 | 255 = 255
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0xFF);
    logical.OR(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 16 | 32 = 48
    register.write(addressDst.value, 0x10);
    register.write(addressSrc.value, 0x20);
    logical.OR(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x30);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 170 | 85 = 255
    register.write(addressDst.value, 0xAA);
    register.write(addressSrc.value, 0x55);
    logical.OR(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 15 | 170 = 175
    register.write(addressDst.value, 0x0F);
    register.write(addressSrc.value, 0xAA);
    logical.OR(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xAF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);
  });

  it('XOR instruction', () => {
    const logical: InstructionLogicalService = TestBed.get(InstructionLogicalService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 255 ^ 255 = 0
    register.write(addressDst.value, 0xFF);
    register.write(addressSrc.value, 0xFF);
    logical.XOR(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test 16 ^ 32 = 48
    register.write(addressDst.value, 0x10);
    register.write(addressSrc.value, 0x20);
    logical.XOR(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x30);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 170 ^ 85 = 255
    register.write(addressDst.value, 0xAA);
    register.write(addressSrc.value, 0x55);
    logical.XOR(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 15 ^ 170 = 165
    register.write(addressDst.value, 0x0F);
    register.write(addressSrc.value, 0xAA);
    logical.XOR(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xA5);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);
  });

  it('COM instruction', () => {
    const logical: InstructionLogicalService = TestBed.get(InstructionLogicalService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test ~255 = 0
    register.write(addressDst.value, 0xFF);
    logical.COM(addressDst);
    expect(register.read(addressDst.value)).toEqual(0x00);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(true);
    expect(flags.negative).toEqual(false);

    // test ~16 = 239
    register.write(addressDst.value, 0x10);
    logical.COM(addressDst);
    expect(register.read(addressDst.value)).toEqual(0xEF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test ~170 = 85
    register.write(addressDst.value, 0xAA);
    logical.COM(addressDst);
    expect(register.read(addressDst.value)).toEqual(0x55);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test ~0 = 255
    register.write(addressDst.value, 0x00);
    logical.COM(addressDst);
    expect(register.read(addressDst.value)).toEqual(0xFF);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);
  });

  it('BITS instruction', () => {
    const logical: InstructionLogicalService = TestBed.get(InstructionLogicalService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 96 set 16 = 112
    register.write(addressDst.value, 0x60);
    register.write(addressSrc.value, 0x10);
    logical.BITS(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x70);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 16 set 32 = 48
    register.write(addressDst.value, 0x10);
    register.write(addressSrc.value, 0x20);
    logical.BITS(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x30);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 170 set 1 = 171
    register.write(addressDst.value, 0xAA);
    register.write(addressSrc.value, 0x01);
    logical.BITS(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xAB);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 15 set 16 = 31
    register.write(addressDst.value, 0x0F);
    register.write(addressSrc.value, 0x10);
    logical.BITS(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x1F);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);
  });

  it('BITC instruction', () => {
    const logical: InstructionLogicalService = TestBed.get(InstructionLogicalService);
    const register: RegisterService = TestBed.get(RegisterService);
    const flags: FlagsService = TestBed.get(FlagsService);

    // test 112 clear 16 = 96
    register.write(addressDst.value, 0x70);
    register.write(addressSrc.value, 0x10);
    logical.BITC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x60);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 48 clear 32 = 16
    register.write(addressDst.value, 0x30);
    register.write(addressSrc.value, 0x20);
    logical.BITC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x10);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);

    // test 171 clear 1 = 170
    register.write(addressDst.value, 0xAB);
    register.write(addressSrc.value, 0x01);
    logical.BITC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0xAA);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(true);

    // test 31 clear 16 = 15
    register.write(addressDst.value, 0x1F);
    register.write(addressSrc.value, 0x10);
    logical.BITC(addressDst, addressSrc);
    expect(register.read(addressDst.value)).toEqual(0x0F);
    expect(flags.carry).toEqual(false);
    expect(flags.zero).toEqual(false);
    expect(flags.negative).toEqual(false);
  });
});

import { TestBed } from '@angular/core/testing';

import { InstructionSystemService } from './instruction-system.service';
import { FlagsService } from '../register/handler/flags.service';

describe('InstructionSystemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const system: InstructionSystemService = TestBed.get(InstructionSystemService);
    expect(system).toBeTruthy();
  });

  it('STOP instruction', () => {
    const system: InstructionSystemService = TestBed.get(InstructionSystemService);

    system.running = true;
    system.STOP();
    expect(system.running).toEqual(false);
  });

  it('NOP instruction', () => {
    const system: InstructionSystemService = TestBed.get(InstructionSystemService);

    system.NOP();
  });

  it('EI instruction', () => {
    const system: InstructionSystemService = TestBed.get(InstructionSystemService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.interruptEnable = false;
    system.EI();
    expect(flags.interruptEnable).toEqual(true);
  });

  it('DI instruction', () => {
    const system: InstructionSystemService = TestBed.get(InstructionSystemService);
    const flags: FlagsService = TestBed.get(FlagsService);

    flags.interruptEnable = true;
    system.DI();
    expect(flags.interruptEnable).toEqual(false);
  });
});

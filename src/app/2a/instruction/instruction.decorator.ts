import { IoAddress, IoAddressType } from '../io/io-address';

export enum ParameterType {
  REGISTER,
  ADDRESS,
  CONST,
  ALL
}

function matches(types: ParameterType[], address: IoAddress, name: string) {
  let error: string;
  for (const type of types) {
    if (typeof type !== 'undefined' && typeof address === 'undefined') {
      error = `missing argument ${name}`;
      continue;
    }

    if (
      (type === ParameterType.REGISTER && address.type !== IoAddressType.REGISTER) ||
      (type === ParameterType.ADDRESS && address.type !== IoAddressType.ADDRESS) ||
      (type === ParameterType.CONST && address.type !== IoAddressType.CONST)
    ) {
      error = `invalid argument ${name}: ${IoAddressType[address.type]} !== [${types.map(t => ParameterType[t])}]`;
      continue;
    }

    return;
  }
  throw new Error(error);
}

export type Parameter = ParameterType | ParameterType[];

export function Instruction(dstType?: Parameter, srcType?: Parameter) {
  // tslint:disable-next-line:only-arrow-functions
  return function(target: any, propertyName: string, propertyDesciptor: PropertyDescriptor) {
    Instruction.instructionToService[propertyName] = target.constructor;

    const method = propertyDesciptor.value;
    propertyDesciptor.value = function(dst?: IoAddress, src?: IoAddress) {
      matches(Array.isArray(dstType) ? dstType : [dstType], dst, 'dst');
      matches(Array.isArray(srcType) ? srcType : [srcType], src, 'src');
      return method.call(this, dst, src);
    };
  };
}
Instruction.instructionToService = {};

import { Injectable } from '@angular/core';

import { IoService } from '../io/io.service';
import { IoAddress } from '../io/io-address';
import { FlagsService } from '../register/handler/flags.service';
import { Instruction, ParameterType } from './instruction.decorator';

@Injectable({
  providedIn: 'root'
})
export class InstructionLogicalService {

  constructor(
    private io: IoService,
    private flags: FlagsService
  ) { }

  private updateFlags(value: number): void {
    this.flags.carry = false;
    this.flags.zero = (value & 0xFF) === 0;
    this.flags.negative = (value & 0x80) !== 0;
  }

  @Instruction(ParameterType.REGISTER, ParameterType.REGISTER)
  AND(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value &= this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER, ParameterType.REGISTER)
  OR(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value |= this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER, ParameterType.REGISTER)
  XOR(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value ^= this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.REGISTER)
  COM(dst: IoAddress) {
    let value = this.io.read(dst);
    value = ~value;

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.ALL, ParameterType.ALL)
  BITS(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value |= this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }

  @Instruction(ParameterType.ALL, ParameterType.ALL)
  BITC(dst: IoAddress, src: IoAddress) {
    let value = this.io.read(dst);
    value &= ~this.io.read(src);

    this.updateFlags(value);
    this.io.write(dst, value);
  }
}

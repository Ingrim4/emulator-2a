import { IoAddress } from '../io/io-address';

export interface ParameterizedInstruction {
  name: string;
  dst?: IoAddress;
  src?: IoAddress;
}

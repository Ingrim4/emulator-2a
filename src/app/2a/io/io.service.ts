import { Injectable } from '@angular/core';
import { MemoryService } from '../memory/memory.service';
import { RegisterService } from '../register/register.service';
import { IoAddress, IoAddressType } from './io-address';

@Injectable({
  providedIn: 'root'
})
export class IoService {

  constructor(
    private memory: MemoryService,
    private register: RegisterService
  ) { }

  read(address: IoAddress): number {
    let indirectAddress: number;
    switch (address.type) {
      case IoAddressType.CONST:
        return address.value;
      case IoAddressType.ADDRESS:
        return this.memory.read(address.value);
      case IoAddressType.REGISTER:
        return this.register.read(address.value);
      case IoAddressType.REGISTER_INDIRECT:
        return this.memory.read(this.register.read(address.value));
      case IoAddressType.REGISTER_INDIRECT_INCREMENT:
        indirectAddress = this.register.read(address.value);
        this.register.write(address.value, indirectAddress + 1);
        return this.memory.read(indirectAddress);
      case IoAddressType.REGISTER_DOUBLE_INDIRECT:
        indirectAddress = this.register.read(address.value);
        this.register.write(address.value, indirectAddress + 1);
        return this.memory.read(this.memory.read(indirectAddress));
      default:
        throw new Error('this shouldn\'t happen');
    }
  }

  write(address: IoAddress, value: number): void {
    let indirectAddress: number;
    switch (address.type) {
      case IoAddressType.CONST:
        throw new Error('can\'t write to IoAddressType.CONST');
      case IoAddressType.ADDRESS:
        this.memory.write(address.value, value);
        break;
      case IoAddressType.REGISTER:
        this.register.write(address.value, value);
        break;
      case IoAddressType.REGISTER_INDIRECT:
        this.memory.write(this.register.read(address.value), value);
        break;
      case IoAddressType.REGISTER_INDIRECT_INCREMENT:
        indirectAddress = this.register.read(address.value);
        this.register.write(address.value, indirectAddress + 1);
        this.memory.write(indirectAddress, value);
        break;
      case IoAddressType.REGISTER_DOUBLE_INDIRECT:
        indirectAddress = this.register.read(address.value);
        this.register.write(address.value, indirectAddress + 1);
        this.memory.write(this.memory.read(indirectAddress), value);
        break;
      default:
        throw new Error('this shouldn\'t happen');
    }
  }
}

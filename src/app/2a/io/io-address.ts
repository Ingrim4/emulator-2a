export enum IoAddressType {
  REGISTER,
  REGISTER_INDIRECT,
  REGISTER_INDIRECT_INCREMENT,
  REGISTER_DOUBLE_INDIRECT,
  ADDRESS,
  CONST
}

export class IoAddress {

  readonly type: IoAddressType;
  readonly value: number;

  constructor(type: IoAddressType, value: number) {
    this.type = type;
    this.value = value;
  }
}

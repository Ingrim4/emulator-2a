import { TestBed } from '@angular/core/testing';

import { IoService } from './io.service';
import { IoAddress, IoAddressType } from './io-address';
import { MemoryService } from '../memory/memory.service';
import { RegisterService } from '../register/register.service';

describe('IoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const io: IoService = TestBed.get(IoService);
    expect(io).toBeTruthy();
  });

  it('shouldn\'t happen', () => {
    const io: IoService = TestBed.get(IoService);
    const address = new IoAddress(null, 0);

    expect(() => io.read(address)).toThrowError('this shouldn\'t happen');
    expect(() => io.write(address, 0)).toThrowError('this shouldn\'t happen');
  });

  it('IoAddressType.CONST', () => {
    const io: IoService = TestBed.get(IoService);
    const address = new IoAddress(IoAddressType.CONST, 0xFF);

    expect(io.read(address)).toEqual(0xFF);
    expect(() => io.write(address, 0x00)).toThrowError('can\'t write to IoAddressType.CONST');
  });

  it('IoAddressType.MEMORY_ADDRESS', () => {
    const memory: MemoryService = TestBed.get(MemoryService);
    const io: IoService = TestBed.get(IoService);
    const address = new IoAddress(IoAddressType.ADDRESS, 0);

    memory.write(0, 0xFF);
    expect(io.read(address)).toEqual(0xFF);

    io.write(address, 0x00);
    expect(memory.read(0)).toEqual(0x00);
  });

  it('IoAddressType.REGISTER', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const io: IoService = TestBed.get(IoService);
    const address = new IoAddress(IoAddressType.REGISTER, 0);

    register.write(0, 0xFF);
    expect(io.read(address)).toEqual(0xFF);

    io.write(address, 0x00);
    expect(register.read(0)).toEqual(0x00);
  });

  it('IoAddressType.REGISTER_INDIRECT', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const memory: MemoryService = TestBed.get(MemoryService);
    const io: IoService = TestBed.get(IoService);
    const address = new IoAddress(IoAddressType.REGISTER_INDIRECT, 0);

    register.write(0, 0x10);

    memory.write(0x10, 0xFF);
    expect(io.read(address)).toEqual(0xFF);

    io.write(address, 0x00);
    expect(memory.read(0x10)).toEqual(0x00);
  });

  it('IoAddressType.REGISTER_INDIRECT_INCREMENT', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const memory: MemoryService = TestBed.get(MemoryService);
    const io: IoService = TestBed.get(IoService);
    const address = new IoAddress(IoAddressType.REGISTER_INDIRECT_INCREMENT, 0);

    register.write(0, 0x10);

    memory.write(0x10, 0xFF);
    expect(io.read(address)).toEqual(0xFF);
    expect(register.read(0)).toEqual(0x11);

    io.write(address, 0xFF);
    expect(memory.read(0x11)).toEqual(0xFF);
    expect(register.read(0)).toEqual(0x12);
  });

  it('IoAddressType.REGISTER_DOUBLE_INDIRECT_INCREMENT', () => {
    const register: RegisterService = TestBed.get(RegisterService);
    const memory: MemoryService = TestBed.get(MemoryService);
    const io: IoService = TestBed.get(IoService);
    const address = new IoAddress(IoAddressType.REGISTER_DOUBLE_INDIRECT, 0);

    register.write(0, 0x10);
    memory.write(0x10, 0x20);
    memory.write(0x11, 0x21);

    memory.write(0x20, 0xFF);
    expect(io.read(address)).toEqual(0xFF);
    expect(register.read(0)).toEqual(0x11);

    io.write(address, 0xFF);
    expect(memory.read(0x21)).toEqual(0xFF);
    expect(register.read(0)).toEqual(0x12);
  });
});

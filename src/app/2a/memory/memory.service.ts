import { Injectable } from '@angular/core';
import { MemoryHandler } from './memory-handler.interface';
import { MemoryHandlerContext } from './memory-handler-context';

@Injectable({
  providedIn: 'root'
})
/**
 * Represents the systems memory
 */
export class MemoryService {

  /**
   * The internal memory, 256 bytes
   */
  public readonly memory: Uint8Array = new Uint8Array(256);

  /**
   * Internal list of all {@link MemoryHandlerContext | MemoryHandlers} that have been registered
   */
  private readonly handlerContexts: MemoryHandlerContext[] = [];

  /**
   * Internal method which throws a {@link RangeError} when the address is outside of the memory
   * @param address address to check
   */
  private checkAddress(address: number) {
    if (address < 0x00 || address > 0xFF) {
      throw new Error(`address(${address}) out of range [0, 255]`);
    }
  }

  /**
   * Register a custom handler to intercept read and write calls on a specific address range
   * @param firstAddress start of address range
   * @param lastAddress end of address range
   * @param handler custom handler
   */
  public registerHandler(firstAddress: number, lastAddress: number, handler: MemoryHandler) {
    this.checkAddress(firstAddress);
    this.checkAddress(lastAddress);

    // check if firstAddress is smaller or equal to lastAddress
    if (firstAddress > lastAddress) {
      throw new Error(`firstAddress(${firstAddress}) should be smaller than lastAddress(${lastAddress})`);
    }

    // check if firstAddress-lastAddress already has a custom handler
    for (const internalHandler of this.handlerContexts) {
      if (Math.max(firstAddress, internalHandler.firstAddress)
        <= Math.min(lastAddress, internalHandler.lastAddress)) {
        throw new Error(`Can't overwrite existing handler at (${
          internalHandler.firstAddress} - ${internalHandler.lastAddress})`);
      }
    }

    // create new context
    const context = new MemoryHandlerContext(
      this.memory.subarray(firstAddress, lastAddress + 1),
      [firstAddress, lastAddress], handler);

    // add handler to handler array
    this.handlerContexts.push(context);
    handler.registered(context);

    // sort array for fast lookup
    this.handlerContexts.sort((a, b) => a.firstAddress - b.firstAddress);
  }

  /**
   * Reads the value of an address in memory
   * @param address address to read from
   * @returns value at address
   */
  public read(address: number): number {
    this.checkAddress(address);

    // check if a handler for the address exists
    for (const context of this.handlerContexts) {
      if (address >= context.firstAddress && address <= context.lastAddress) {
        return context.handler.read(context, address);
      }
    }

    return this.memory[address];
  }

  /**
   * Writes the a value to an address in memory
   * @param address address to write to
   * @param value value to write
   */
  public write(address: number, value: number): void {
    this.checkAddress(address);

    // check if a handler for the address exists
    for (const context of this.handlerContexts) {
      if (address >= context.firstAddress && address <= context.lastAddress) {
        return context.handler.write(context, address, value);
      }
    }

    this.memory[address] = value;
  }
}

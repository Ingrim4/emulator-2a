import { TestBed } from '@angular/core/testing';

import { IoRegisterService } from './io-register.service';
import { MemoryService } from '../memory.service';

describe('IoRegisterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const ioRegister: IoRegisterService = TestBed.get(IoRegisterService);
    expect(ioRegister).toBeTruthy();
  });

  it('should read and write', () => {
    const ioRegister: IoRegisterService = TestBed.get(IoRegisterService);
    const memory: MemoryService = TestBed.get(MemoryService);

    ioRegister.writeInput(0xFC, 0xFC);
    ioRegister.writeInput(0xFF, 0xFF);
    expect(memory.read(0xFC)).toEqual(0xFC);
    expect(memory.read(0xFD)).toEqual(0x00);
    expect(memory.read(0xFF)).toEqual(0xFF);

    expect(() => memory.write(0xFD, 0x00)).toThrowError('can\'t write to input register');
    memory.write(0xFE, 0xFE);
    memory.write(0xFF, 0xFF);
  });
});

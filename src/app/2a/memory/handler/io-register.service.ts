import { Injectable } from '@angular/core';
import { MemoryHandler } from '../memory-handler.interface';
import { MemoryHandlerContext } from '../memory-handler-context';
import { MemoryService } from '../memory.service';

@Injectable({
  providedIn: 'root'
})
export class IoRegisterService implements MemoryHandler {

  private input: Uint8Array = new Uint8Array(4);

  constructor(
    private memory: MemoryService
  ) {
    this.memory.registerHandler(0xFC, 0xFF, this);
  }

  writeInput(address: number, value: number) {
    this.input[address - 0xFC] = value;
  }

  registered(context: MemoryHandlerContext): void {
  }

  read(context: MemoryHandlerContext, address: number): number {
    address = context.translateAddress(address);
    return this.input[address];
  }

  write(context: MemoryHandlerContext, address: number, value: number): void {
    if (address < 0xFE) {
      throw new Error('can\'t write to input register');
    }
    context.write(address, value);
  }
}

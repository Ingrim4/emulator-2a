import { Injectable } from '@angular/core';
import { MemoryHandlerContext } from '../memory-handler-context';
import { MemoryHandler } from '../memory-handler.interface';
import { MemoryService } from '../memory.service';

@Injectable({
  providedIn: 'root'
})
export class Mr2da2Service implements MemoryHandler {

  private register: Uint8Array = new Uint8Array(2);

  constructor(
    private memory: MemoryService
  ) {
    this.memory.registerHandler(0xF0, 0xF7, this);
  }

  writeAnalogInput(address: number, value: number) {
    if (address < 0 || address > 1) {
      throw new Error('internal error see console');
    }
    this.register[address] = value;
  }

  registered(context: MemoryHandlerContext): void {
  }

  read(context: MemoryHandlerContext, address: number): number {
    if (address === 0xF1) {
      let value = 0;
      if (this.register[0] > context.read(0xF0)) {
        value |= 0x08;
      }
      if (this.register[1] > context.read(0xF1)) {
        value |= 0x10;
      }
      return value;
    } else {
      throw new Error(`address(${address}) read not supported by this emulator`);
    }
  }

  write(context: MemoryHandlerContext, address: number, value: number): void {
    if (address === 0xF0 || address === 0xF1) {
      context.write(address, value);
    } else {
      throw new Error(`address(${address}) write not supported by this emulator`);
    }
  }
}

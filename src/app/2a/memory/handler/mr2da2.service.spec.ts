import { TestBed } from '@angular/core/testing';

import { Mr2da2Service } from './mr2da2.service';
import { MemoryService } from '../memory.service';

describe('Mr2da2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const mr2da2: Mr2da2Service = TestBed.get(Mr2da2Service);
    expect(mr2da2).toBeTruthy();
  });

  it('should read and write', () => {
    const mr2da2: Mr2da2Service = TestBed.get(Mr2da2Service);
    const memory: MemoryService = TestBed.get(MemoryService);

    mr2da2.writeAnalogInput(0, 0x10);
    mr2da2.writeAnalogInput(1, 0x20);
    expect(() => mr2da2.writeAnalogInput(2, 0)).toThrowError('internal error see console');

    memory.write(0xF0, 0x08);
    memory.write(0xF1, 0x40);
    expect(() => memory.write(0xF2, 0x00)).toThrowError('address(242) write not supported by this emulator');

    expect(memory.read(0xF1)).toEqual(0x08);
    expect(() => memory.read(0xF0)).toThrowError('address(240) read not supported by this emulator');

    memory.write(0xF0, 0x20);
    memory.write(0xF1, 0x10);
    expect(memory.read(0xF1)).toEqual(0x10);
  });
});

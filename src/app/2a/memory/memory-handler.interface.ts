import { MemoryHandlerContext } from './memory-handler-context';

/**
 * Handler used to intercept calls to the {@link MemoryService}'s read and write method
 */
export interface MemoryHandler {

  registered(context: MemoryHandlerContext): void;

  /**
   * Reads the value of an address in memory
   * @param context this handlers context
   * @param address address to read from
   * @returns value at address
   */
  read(context: MemoryHandlerContext, address: number): number;

  /**
   * Writes a value to an address in memory
   * @param context this handlers context
   * @param address address to write to
   * @param value value to write
   */
  write(context: MemoryHandlerContext, address: number, value: number): void;
}

import { MemoryHandler } from './memory-handler.interface';

/**
 * Enables {@link MemoryHandler} to interact with the system memory
 */
export class MemoryHandlerContext {

  /**
   * chunk of memory
   */
  private readonly memoryChunk: Uint8Array;

  /**
   * address range the memory chunk represents
   */
  private readonly range: [number, number];

  /**
   * handler this context belongs to
   */
  public readonly handler: MemoryHandler;

  /**
   * Creates a new {@link MemoryHandlerContext}
   * @param memoryChunk chunk of memory
   * @param range address range the memory chunk represents
   * @param handler handler this context belongs to
   */
  constructor(memoryChunk: Uint8Array, range: [number, number], handler: MemoryHandler) {
    this.memoryChunk = memoryChunk;
    this.range = range;
    this.handler = handler;
  }

  get firstAddress(): number {
    return this.range[0];
  }

  get lastAddress(): number {
    return this.range[1];
  }

  /**
   * Translates a global memory address to a local memory address
   * @param address global address
   * @returns local address
   */
  translateAddress(address: number): number {
    const index = address - this.range[0];
    if (index < 0 || index >= this.memoryChunk.length) {
      throw new Error(`address(${address}) out of range [${this.range[0]}, ${this.range[1]}]`);
    }
    return index;
  }

  /**
   * Reads the value of an address in assigned memory chunk
   * @param address address to read from
   * @returns value read at address
   */
  public read(address: number): number {
    return this.memoryChunk[this.translateAddress(address)];
  }

  /**
   * Writes a value to an address in assigned memory chunk
   * @param address address to write to
   * @param value value to write
   */
  public write(address: number, value: number) {
    this.memoryChunk[this.translateAddress(address)] = value;
  }
}

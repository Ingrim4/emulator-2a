import { MemoryHandlerContext } from './memory-handler-context';

describe('MemoryHandlerContext', () => {
  it('should create an instance', () => {
    const context = new MemoryHandlerContext(null, [ 2, 5 ], null);

    expect(context).toBeTruthy();
    expect(context.firstAddress).toEqual(2);
    expect(context.lastAddress).toEqual(5);
  });

  it('should read and write', () => {
    const memory = new Uint8Array(4);
    const context = new MemoryHandlerContext(memory, [ 2, 5 ], null);

    context.write(3, 1);
    context.write(4, 2);
    context.write(5, 3);

    expect(memory).toEqual(new Uint8Array([ 0, 1, 2, 3 ]));

    expect(context.read(2)).toEqual(0);
    expect(context.read(3)).toEqual(1);
    expect(context.read(4)).toEqual(2);
    expect(context.read(5)).toEqual(3);

    expect(() => context.read(0)).toThrowError('address(0) out of range [2, 5]');
    expect(() => context.read(6)).toThrowError('address(6) out of range [2, 5]');
  });
});

import { TestBed } from '@angular/core/testing';

import { MemoryService } from './memory.service';
import { MemoryHandlerContext } from './memory-handler-context';
import { MemoryHandler } from './memory-handler.interface';

class SimpleMemoryHandler implements MemoryHandler {
  registered(context: MemoryHandlerContext) {
  }
  read(context: MemoryHandlerContext, address: number): number {
    return context.read(address);
  }
  write(context: MemoryHandlerContext, address: number, value: number): void {
    context.write(address, value);
  }
}

describe('MemoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const memory: MemoryService = TestBed.get(MemoryService);
    expect(memory).toBeTruthy();
  });

  it('should read and write', () => {
    const memory: MemoryService = TestBed.get(MemoryService);

    memory.write(0, 1);
    memory.write(1, 2);
    memory.write(2, 3);
    memory.write(0xFD, 4);
    memory.write(0xFE, 5);
    memory.write(0xFF, 6);

    expect(memory.read(0)).toEqual(1);
    expect(memory.read(1)).toEqual(2);
    expect(memory.read(2)).toEqual(3);
    expect(memory.read(3)).toEqual(0);
    expect(memory.read(0xFC)).toEqual(0);
    expect(memory.read(0xFD)).toEqual(4);
    expect(memory.read(0xFE)).toEqual(5);
    expect(memory.read(0xFF)).toEqual(6);

    expect(() => memory.read(0x100)).toThrowError('address(256) out of range [0, 255]');
    expect(() => memory.read(-0x1)).toThrowError('address(-1) out of range [0, 255]');
  });

  it('should register handler', () => {
    const memory: MemoryService = TestBed.get(MemoryService);

    expect(() => memory.registerHandler(2, 0, null))
      .toThrowError('firstAddress(2) should be smaller than lastAddress(0)');

    memory.registerHandler(2, 3, new SimpleMemoryHandler());

    expect(() => memory.registerHandler(0, 2, null))
      .toThrowError('Can\'t overwrite existing handler at (2 - 3)');

    memory.registerHandler(5, 6, new SimpleMemoryHandler());
  });

  it('should read and write from handler', () => {
    const memory: MemoryService = TestBed.get(MemoryService);
    memory.registerHandler(2, 3, new SimpleMemoryHandler());

    memory.write(0, 1);
    memory.write(1, 2);
    memory.write(2, 3);
    memory.write(3, 4);
    memory.write(4, 5);

    expect(memory.read(0)).toEqual(1);
    expect(memory.read(1)).toEqual(2);
    expect(memory.read(2)).toEqual(3);
    expect(memory.read(3)).toEqual(4);
    expect(memory.read(4)).toEqual(5);

    expect(memory.memory.slice(0, 5))
      .toEqual(new Uint8Array([ 1, 2, 3, 4, 5 ]));
  });
});

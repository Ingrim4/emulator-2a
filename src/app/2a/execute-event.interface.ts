export interface ExecuteEvent {
  programCounter: number;
  error?: Error;
}

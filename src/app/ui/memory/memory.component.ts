import { Component } from '@angular/core';
import { MemoryService } from 'src/app/2a/memory/memory.service';
import { StackService } from 'src/app/2a/register/handler/stack.service';

@Component({
  selector: 'app-memory',
  templateUrl: './memory.component.html',
  styleUrls: ['./memory.component.scss']
})
export class MemoryComponent {

  readonly keys = Array(16).fill(0).map((_, i) => i);

  constructor(
    private memory: MemoryService,
    private stack: StackService
  ) { }

  isStack(value: number) {
    return this.stack.address - this.stack.size < value && value <= this.stack.address;
  }

  memorySection(section: number) {
    return Array.from(this.memory.memory.slice(section * 16, (section + 1) * 16));
  }
}

import { IntegerPipe } from './integer.pipe';

describe('IntegerPipe', () => {
  it('create an instance', () => {
    const pipe = new IntegerPipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform', () => {
    const pipe = new IntegerPipe();

    expect(pipe.transform(16)).toEqual('10');

    pipe.radix = 10;

    expect(pipe.transform(16)).toEqual('16');
    expect(pipe.transform(0xFE)).toEqual('-2');
  });
});

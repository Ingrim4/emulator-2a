import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'integer'
})
export class IntegerPipe implements PipeTransform {

  radix = 16;

  transform(value: number, args?: any): any {
    if (typeof value === 'number') {
      if (this.radix === 10) {
        value = value > 127 ? value - 256 : value;
      }
      return value.toString(this.radix).toUpperCase();
    }
    throw new Error('invalid type for IntegerPipe');
  }
}

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { CodemirrorComponent } from '@ctrl/ngx-codemirror';
import { Subscription } from 'rxjs';

import '../../mrasm/codemirror.mrasm';

import { MrasmParserService } from 'src/app/mrasm/mrasm-parser.service';
import { CoreService } from 'src/app/2a/core.service';
import { ParsedInstruction, ParsedAssemblerInstruction } from 'src/app/mrasm/mrasm-parser.interface';
import { ExecuteEvent } from 'src/app/2a/execute-event.interface';
import { IntegerPipe } from '../integer.pipe';
import { Mr2da2Service } from 'src/app/2a/memory/handler/mr2da2.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit, OnDestroy {

  @ViewChild('editor')
  editor: CodemirrorComponent;

  private lineWidget: CodeMirror.LineWidget;

  private instructions: ParsedInstruction[];
  private subscription: Subscription;

  constructor(
    private parser: MrasmParserService,
    public core: CoreService,
    private pipe: IntegerPipe,
    private mr2da2: Mr2da2Service
  ) { }

  ngOnInit(): void {
    this.subscription = this.core.execute.subscribe(({ programCounter, error }: ExecuteEvent) => {
      const cm = this.editor.codeMirror;
      if ('activeline' in cm.state) {
        cm.removeLineClass(cm.state.activeline, 'gutter', 'CodeMirror-activeline');
        cm.removeLineClass(cm.state.activeline, 'background', 'CodeMirror-activeline');
      }

      const instruction = this.instructions[programCounter];
      if (typeof instruction !== 'undefined') {
        const line = cm.state.activeline = instruction.originalLine;
        cm.addLineClass(line, 'gutter', 'CodeMirror-activeline');
        cm.addLineClass(line, 'background', 'CodeMirror-activeline');
      }

      if (error) {
        this.error(error);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private error(error: Error) {
    const cm = this.editor.codeMirror;

    const node = document.createElement('span');
    node.textContent = `${error.name}: ${error.message}`;
    node.style.paddingLeft = '4px';

    const line = 'activeline' in cm.state ? cm.state.activeline : 0;
    this.lineWidget = cm.addLineWidget(line, node);

    console.error(error);
  }

  radix(event: Event) {
    this.pipe.radix = parseInt((event.target as HTMLSelectElement).value, 10);
  }

  input(event: Event, address: number) {
    const value = Math.max(0, Math.min(2.55, (event.target as HTMLInputElement).valueAsNumber)) * 100;
    if (!isNaN(value)) {
      this.mr2da2.writeAnalogInput(address, value);
    }
  }

  private getProgramOrigin(assemblerInstructions: ParsedAssemblerInstruction[]) {
    for (const assemblerInstruction of assemblerInstructions) {
      const exec = /^ORG\s+(\d+)\b/i.exec(assemblerInstruction.text);
      if (exec) {
        return parseInt(exec[1], 10);
      }
    }
    return 0;
  }

  parse() {
    if (this.lineWidget) {
      this.lineWidget.clear();
      delete this.lineWidget;
    }

    const cm = this.editor.codeMirror;
    if ('activeline' in cm.state) {
      cm.removeLineClass(cm.state.activeline, 'gutter', 'CodeMirror-activeline');
      cm.removeLineClass(cm.state.activeline, 'background', 'CodeMirror-activeline');
      delete cm.state.activeline;
    }

    try {
      const parsed = this.parser.parse(this.editor.value);
      this.instructions = parsed.instructions;
      this.core.load(this.instructions, this.getProgramOrigin(parsed.assemblerInstructions));
    } catch (error) {
      this.error(error);
    }
  }
}

import { Component } from '@angular/core';
import { RegisterService } from '../../2a/register/register.service';
import { IoRegisterService } from 'src/app/2a/memory/handler/io-register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  constructor(
    public register: RegisterService,
    private io: IoRegisterService
  ) { }

  input(event: Event, address: number) {
    const value = Math.max(0, Math.min(255, (event.target as HTMLInputElement).valueAsNumber));
    if (!isNaN(value)) {
      this.io.writeInput(address, value);
    }
  }
}

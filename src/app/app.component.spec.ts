import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { EditorComponent } from './ui/editor/editor.component';
import { MemoryComponent } from './ui/memory/memory.component';
import { RegisterComponent } from './ui/register/register.component';
import { FormsModule } from '@angular/forms';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { IntegerPipe } from './ui/integer.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        EditorComponent,
        MemoryComponent,
        RegisterComponent,
        IntegerPipe
      ],
      imports: [
        FormsModule,
        CodemirrorModule
      ],
      providers: [
        IntegerPipe
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'emulator'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('emulator');
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';

import { AppComponent } from './app.component';
import { EditorComponent } from './ui/editor/editor.component';
import { MemoryComponent } from './ui/memory/memory.component';
import { RegisterComponent } from './ui/register/register.component';
import { IntegerPipe } from './ui/integer.pipe';

@NgModule({
  declarations: [
    AppComponent,
    EditorComponent,
    MemoryComponent,
    RegisterComponent,
    IntegerPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CodemirrorModule
  ],
  exports: [
    IntegerPipe
  ],
  providers: [
    IntegerPipe
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
